package com.example.mvpproject.ui;

public interface MovieView {
    void onGetMovieName(String name);
    void onGetMovieId(String id);
}
