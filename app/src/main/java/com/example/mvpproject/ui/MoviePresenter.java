package com.example.mvpproject.ui;

import com.example.mvpproject.Pojo.MovieModel;

class MoviePresenter {
    /*
        * Processing And Logic Code Here
    */
    private MovieView view;

    MoviePresenter(MovieView view) {
        this.view = view;
    }

    private MovieModel getMovieNameFromDataBase() {
        return new MovieModel("Nasser Movie","1");
    }

    void getMovieName() {
        view.onGetMovieName(getMovieNameFromDataBase().getName());
    }

    void getMovieId() {
        view.onGetMovieId(getMovieNameFromDataBase().getId());
    }
}
