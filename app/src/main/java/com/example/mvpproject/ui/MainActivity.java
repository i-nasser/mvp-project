package com.example.mvpproject.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.mvpproject.R;

public class MainActivity extends AppCompatActivity implements MovieView{

    /* MVP -> (Model View Presenter) Architecture Design Pattern *
    * Model -> Some Data From DB
    * View -> Layout XML And Activity (Control View UI Only)
    * Presenter -> Class And Class Interface (هيبقي حلقة وصل بين الاكتيفتي والبريزنتر)
    * */

    private MoviePresenter presenter;
    private TextView textView,textViewID;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MoviePresenter(this);

        textView = findViewById(R.id.textView);
        textViewID = findViewById(R.id.textViewID);
        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.getMovieName();
                presenter.getMovieId();
            }
        });
    }

    @Override
    public void onGetMovieName(String name) {
        textView.setText(name);
    }

    @Override
    public void onGetMovieId(String id) {
        textViewID.setText(id);
    }
}
